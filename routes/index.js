module.exports = {
    authRoute: require('./auth.route'),
    refreshRoute: require('./refresh.route'),
    logoutRoute: require('./logout.route'),
    userRoute: require('./user.route'),
};
