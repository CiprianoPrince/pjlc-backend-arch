module.exports = {
    deleteImageSync: require('./deleteImageSync.helper'),
    extractBaseName: require('./extractBaseName.helper'),
    generateMessage: require('./generateMessage.helper'),
    getModelName: require('./getModelName.helper'),
    sendResponse: require('./sendResponse.helper'),
};
