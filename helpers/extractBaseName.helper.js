const extractBaseName = (fileName) => {
    const extractedBaseName = fileName.split('.')?.[0];
    return extractedBaseName;
};

module.exports = extractBaseName;
