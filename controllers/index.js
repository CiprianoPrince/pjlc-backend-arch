module.exports = {
    authController: require('./auth.controller'),
    refreshTokenController: require('./refreshToken.controller'),
    logoutController: require('./logout.controller'),
    userController: require('./user.controller'),
};
